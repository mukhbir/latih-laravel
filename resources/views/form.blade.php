@extends('layout.master')

@section('judul')
<h3>Buat Account Baru</h3>
<h4>Sign Up Form</h4>
   
@endsection

@section('content')
<form action="/welcome" method="post">
    @csrf
    <label for="">Nama Lengkap</label><br><br>
    <input type="text" name="nama"><br><br>
    <label>Alamat</label><br><br>
    <textarea name="address" id="" cols="30" rows="10"></textarea><br><br>
    <input type="submit" value="Kirim">
</form>

@endsection

