@extends('layout.master')

@section('judul')
<h1>Media Online</h1>
<h2>Sosial Media Developer</h2>    
@endsection

@section('content')
<p>Belajar dan berbagi agar hidup menjadi lebih baik</p>

<h3>Benefit Join di Media Online</h3>
    <ul>
        <li><p>Mendapatkan motivasi dari sesama para Developer</p></li>
        <li><p>Sharing knowledge</p></li>
        <li><p>Dibuat oleh calon web developer terbaik</p></li>
    </ul>  

<h3>Cara Bergabung ke Media Online</h3>
    <ol type="1">
        <li>Mengunjungi Website ini</li>
        <li>Mendaftarkan di <a href="/form">Form Sign Up</a></li>
        <li>Selesai</li>
     </ol>
@endsection

