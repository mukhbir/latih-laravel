@extends('layout.master')

@section('judul')
<h3>Halaman Form</h3>
   
@endsection

@section('content')

<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Deskripsi</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($kategori as $key->$item)
        <tr>
            <td>{{$key + 1}}</td>
            <td>{{$item->nama}}</td>
            <td>{{$item->deskripsi}}</td>
        </tr>
        @empty
        <tr>
            <td>Data Masih Kosong</td>
        </tr>
        @endforelse
    </tbody>
  </table>

  @endsection