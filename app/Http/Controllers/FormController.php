<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function bio(){
        return view('form');
    }

    public function kirim(Request $request){
        $nama = $request['nama'];
        $alamat = $request['address'];
        return view('welcome', compact('nama','alamat'));
    }
}

